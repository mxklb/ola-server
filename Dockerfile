FROM ubuntu:22.04@sha256:ed1544e454989078f5dec1bfdabd8c5cc9c48e0705d07b678ab6ae3fb61952d2

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y python3-pip \ 
    libcppunit-dev libcppunit-dev uuid-dev pkg-config libncurses5-dev libtool \
    autoconf automake g++ libmicrohttpd-dev libmicrohttpd12 protobuf-compiler \
    libprotobuf-lite23 python3-protobuf libprotobuf-dev libprotoc-dev zlib1g-dev \
    bison flex make libftdi-dev libftdi1 libusb-1.0-0-dev liblo-dev \
    libavahi-client-dev git avahi-daemon supervisor

RUN git clone -b 0.10.9 --single-branch https://github.com/OpenLightingProject/ola.git /tmp/ola && \ 
    cd /tmp/ola && \
    autoreconf -i && \
    ./configure \
        --enable-python-libs \
        --disable-all-plugins \
        --enable-libusb \
        --enable-usbpro \
        --enable-osc \
        --enable-artnet \
        --enable-e131 \
        --disable-root-check && \
    make -j$(nproc --all) && \
    make install && \
    ldconfig

# This works with python2 ..
#RUN export PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.7/dist-packages
# Python3 Issue:
# TODO: Add equivalent lib search path for python3, actual workaround:
# - use `sys.path.append('/usr/local/lib/python3.8/site-packages')` in scripts to find ola modules.

RUN apt-get autoremove && apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD start.sh /start.sh
ADD supervisord.conf /etc/supervisord.conf

RUN cd / && \
    mkdir -p /var/log/supervisord && \
    mkdir -p /scripts && \
    chmod a+x /start.sh

EXPOSE 9090

ENTRYPOINT ["/start.sh"]
