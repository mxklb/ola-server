#!/bin/bash

# Print commands
set -x

# Make the container names unique
export COMPOSE_PROJECT_NAME=$(git rev-parse --short HEAD)

# the docker-compose file uses $IMAGE to set the project image
docker_compose_cmd="docker-compose -f docker-compose.ci.yml"
#${docker_compose_cmd} build
# Run service containers, except for test container
${docker_compose_cmd} up -d --build

sleep 5
# Run the tests
#${docker_compose_cmd} run --rm tests
# keep the exit code from tests while we clean up the containers
#exit_code=$?

# Clean up
${docker_compose_cmd} down
exit_code=$?

exit ${exit_code}